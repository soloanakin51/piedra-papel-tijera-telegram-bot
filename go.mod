module telbot.hackaton2

go 1.21.6

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
