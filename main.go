package main

import (
	// "log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var numericKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/start"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/ayuda"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/saludo"),
	),
)

// func main() {
// 	// Reemplaza "TU_TOKEN" con el token de tu bot obtenido al crearlo en BotFather.
// 	bot, err := tgbotapi.NewBotAPI("6747023492:AAFZbBOqpvvBGxCrM5dxObmrW5tOLchGWck")
// 	if err != nil {
// 		log.Panic(err)
// 	}

// 	bot.Debug = true

// 	log.Printf("Conectado a la cuenta de %s", bot.Self.UserName)

// 	u := tgbotapi.NewUpdate(0)
// 	u.Timeout = 60

// 	updates, err := bot.GetUpdatesChan(u)

// 	for update := range updates {
// 		if update.Message == nil {
// 			continue
// 		}

// 		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

// 		if update.Message.IsCommand() {
// 			handleCommand(bot, update.Message)
// 		}
// 	}
// }

// func sendMsg(bot *tgbotapi.BotAPI, msg tgbotapi.MessageConfig, keyboard bool) {
// 	if keyboard {
// 		msg.ReplyMarkup = numericKeyboard
// 	} else {
// 		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
// 	}
// 	bot.Send(msg)
// }

// func handleCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message) {
// 	switch message.Command() {
// 	case "start":
// 		msg := tgbotapi.NewMessage(message.Chat.ID, "¡Hola! Soy tu bot. ¿En qué puedo ayudarte?")
// 		sendMsg(bot, msg, true)
// 	case "ayuda":
// 		msg := tgbotapi.NewMessage(message.Chat.ID, "Puedo ayudarte con diferentes comandos. /start para saludar y /ayuda para obtener ayuda.")
// 		sendMsg(bot, msg, true)
// 	case "saludo":
// 		msg := tgbotapi.NewMessage(message.Chat.ID, "Salu2")
// 		sendMsg(bot, msg, false)
// 	default:
// 		// Manejar comandos desconocidos o no especificados
// 		msg := tgbotapi.NewMessage(message.Chat.ID, "Comando desconocido. Usa /ayuda para obtener una lista de comandos.")
// 		bot.Send(msg)
// 	}
// }
