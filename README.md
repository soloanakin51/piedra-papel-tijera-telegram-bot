# Bot de Telegram Piedra Papel o Tijera

Es un bot para jugar el juego de a dos personas, desde cada cuenta de telegram.

### Pasos para correr:

1. **Crear un nuevo bot en Telegram:**
   - Abre Telegram y busca al bot llamado "BotFather".
   - Inicia una conversación y sigue las instrucciones para crear un nuevo bot.
   - Anota el token del bot que se te proporcionará.

2. **Clona el repo**

3. **Crea el archivo de configugración**
    - ppt.toml:
    ```toml
    [telegram]
    token="TU_TOKEN"
    # Reemplaza "TU_TOKEN" con el token que obtuviste al crear el bot en BotFather.
    ```

4. **Corre el bot**
    ```sh
    go run ppt.go
    ```