package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pelletier/go-toml"
)

var jugador int64 = 0
var usuario string = ""
var jugada string = ""

var pptKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.KeyboardButton{},
		tgbotapi.NewKeyboardButton("🪨"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("🧻"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("✂️"),
	),
)

func main() {
	// Ruta al archivo de config
	rutaConfig := "ppt.toml"

	// Cargar el archivo de config
	config, err := toml.LoadFile(rutaConfig)
	if err != nil {
		fmt.Println("Error al cargar ppt.toml:", err)
		return
	}

	// Extraer valores del archivo de config
	telegramToken := config.Get("telegram.token").(string)
	fmt.Println("Valor extraído de ppt.toml:", telegramToken)

	// Reemplaza "TU_TOKEN" con el token que obtuviste al crear el bot en BotFather.
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true
	log.Printf("Conectado a la cuenta de %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		if update.Message.IsCommand() {
			handleCommand(bot, update.Message)
		} else {
			if update.Message.Text == "piedra" || update.Message.Text == "papel" || update.Message.Text == "tijera" || update.Message.Text == "🪨" || update.Message.Text == "🧻" || update.Message.Text == "✂️" {
				playGame(bot, update.Message)
			}
		}
	}
}

func handleCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message) {
	switch message.Command() {
	case "start":
		msg := tgbotapi.NewMessage(message.Chat.ID, "¡Bienvenido! Vamos a jugar a piedra, papel o tijera. ¿Listo?")
		// Reemplaza "RUTA_DEL_GIF" con la ruta al archivo GIF que deseas enviar.
		gifPath := "intro.gif"

		// Abre el archivo GIF
		gifFile, err := os.Open(gifPath)
		if err != nil {
			log.Panic(err)
		}

		// Crea un nuevo mensaje de animación
		animation := tgbotapi.NewAnimationUpload(message.Chat.ID, tgbotapi.FileReader{
			Name:   "animated.gif",
			Reader: gifFile,
			Size:   -1,
		})

		// Envía el mensaje de animación al chat
		bot.Send(animation)
		msg.ReplyMarkup = pptKeyboard
		bot.Send(msg)
	case "play":
		playGame(bot, message)
	default:
		msg := tgbotapi.NewMessage(message.Chat.ID, "Comando desconocido. Usa /start para comenzar o /play para jugar.")
		bot.Send(msg)
	}
}

func playGame(bot *tgbotapi.BotAPI, message *tgbotapi.Message) {
	fmt.Println("FROM: ", message.From)
	// options := []string{"piedra", "papel", "tijera"}
	userChoice := strings.ToLower(message.CommandArguments())
	if userChoice == "" {
		userChoice = message.Text
	}

	if userChoice != "piedra" && userChoice != "papel" && userChoice != "tijera" && userChoice != "🪨" && userChoice != "🧻" && userChoice != "✂️" {
		msg := tgbotapi.NewMessage(message.Chat.ID, "Por favor, elige piedra, papel o tijera.")
		bot.Send(msg)
		return
	}

	if jugador == 0 {
		jugador = message.Chat.ID
		jugada = userChoice
		usuario = message.From.UserName
		msg := tgbotapi.NewMessage(message.Chat.ID, "Espere")
		bot.Send(msg)
	} else {
		if jugador != message.Chat.ID {
			botChoice := jugada

			result := determineWinner(userChoice, botChoice)

			response := "Elegiste " + userChoice + "\n" + usuario + " eligió " + botChoice + "\nResultado: " + result

			resultprev := determineWinner(botChoice, userChoice)

			responseprev := "Elegiste " + botChoice + "\n" + message.From.UserName + " eligió " + userChoice + "\nResultado: " + resultprev

			msg := tgbotapi.NewMessage(message.Chat.ID, response)
			bot.Send(msg)
			msgprev := tgbotapi.NewMessage(jugador, responseprev)
			bot.Send(msgprev)

			sendImgResult(bot, jugador, botChoice, message.Chat.ID, userChoice)

			jugador = 0
			jugada = ""
			usuario = ""
		} else {
			delete := tgbotapi.NewDeleteMessage(message.Chat.ID, message.MessageID)
			bot.Send(delete)
			msg := tgbotapi.NewMessage(message.Chat.ID, "Invita a alguien a jugar desde otro dispositivo.")
			m, _ := bot.Send(msg)
			time.Sleep(time.Second * 5)
			delete = tgbotapi.NewDeleteMessage(m.Chat.ID, m.MessageID)
			bot.Send(delete)
		}
	}
}

func determineWinner(userChoice, botChoice string) string {
	if userChoice == botChoice {
		return "¡Es un empate!"
	} else if (userChoice == "piedra" && botChoice == "tijera") ||
		(userChoice == "papel" && botChoice == "piedra") ||
		(userChoice == "tijera" && botChoice == "papel") ||
		(userChoice == "🪨" && botChoice == "✂️") ||
		(userChoice == "🧻" && botChoice == "🪨") ||
		(userChoice == "✂️" && botChoice == "🧻") ||
		(userChoice == "piedra" && botChoice == "✂️") ||
		(userChoice == "papel" && botChoice == "🪨") ||
		(userChoice == "tijera" && botChoice == "🧻") ||
		(userChoice == "🪨" && botChoice == "tijera") ||
		(userChoice == "🧻" && botChoice == "piedra") ||
		(userChoice == "✂️" && botChoice == "papel") {
		return "¡Ganaste!"
	}
	return "¡Perdiste!"
}

func sendImgResult(bot *tgbotapi.BotAPI, id1 int64, choice1 string, id2 int64, choice2 string) {
	imgPath := ""
	if choice1 == "piedra" || choice1 == "🪨" {
		if choice2 == "piedra" || choice2 == "🪨" {
			imgPath = "rr.png"
		} else if choice2 == "papel" || choice2 == "🧻" {
			imgPath = "rp.png"
		} else if choice2 == "tijera" || choice2 == "✂️" {
			imgPath = "rs.png"
		}
	} else if choice1 == "papel" || choice1 == "🧻" {
		if choice2 == "piedra" || choice2 == "🪨" {
			imgPath = "pr.png"
		} else if choice2 == "papel" || choice2 == "🧻" {
			imgPath = "pp.png"
		} else if choice2 == "tijera" || choice2 == "✂️" {
			imgPath = "ps.png"
		}
	} else if choice1 == "tijera" || choice1 == "✂️" {
		if choice2 == "piedra" || choice2 == "🪨" {
			imgPath = "sr.png"
		} else if choice2 == "papel" || choice2 == "🧻" {
			imgPath = "sp.png"
		} else if choice2 == "tijera" || choice2 == "✂️" {
			imgPath = "ss.png"
		}
	}
	// Abre el archivo
	imgFile, err := os.Open(imgPath)
	if err != nil {
		log.Panic(err)
	}

	// Crea un nuevo mensaje de foto
	result := tgbotapi.NewPhotoUpload(id1, tgbotapi.FileReader{
		Name:   "result.png",
		Reader: imgFile,
		Size:   -1,
	})

	// Envía el mensaje de foto al chat
	bot.Send(result)

	// Crea un nuevo mensaje de foto
	result = tgbotapi.NewPhotoUpload(id2, tgbotapi.FileReader{
		Name:   "result.png",
		Reader: imgFile,
		Size:   -1,
	})

	// Envía el mensaje de foto al chat
	bot.Send(result)
}
